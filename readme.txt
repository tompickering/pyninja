Requirements:

You will need to install the pygame library.

Acknowledgements:

Pygame
'Resagokr' font by Gluk, sourced from www.fontspace.com

Sprites:
Shuriken: https://opengameart.org/users/loel (https://opengameart.org/sites/default/files/shuriken%20pixel%20art_0.png)
Box: https://opengameart.org/users/alekei https://opengameart.org/sites/default/files/box%20animation.zip
