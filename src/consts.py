WIN_SZ = (1000,640)
WIN_POS= (20,30)

FPS    = 40   # Max Frames Per Second
BS     = 42   # Block size

ANTIALIAS = True

TIMESTEP = 1

FONT_NAME_MAIN  = 'Resagokr.otf'
FONT_NAME_BOLD  = 'ResagokrBold.otf'
FONT_NAME_LIGHT = 'ResagokrLight.otf'

FIRST_BOX_Y = 250
BOX_YSEP = 5
BOX_XPAD = 10
BOX_YPAD = 4

FONTSIZE_S = 14
FONTSIZE_M = 23
FONTSIZE_L = 32
