from level import Level

from consts import BS

from things.entry import Entry
from things.lever import Lever
from things.target import Target
from things.box import Box
from things.guard import Guard


class TestLevel00(Level):
    def __init__(self):
        self.size = (12, 12)
        self.h_walls = {
            (6,4,6),
            (1,5,6),
            (1,1,4),
            (1,4,4),
            (10,11,1),
            (10,10,1),
            (6,1,6),
            (10,5,1),
            (8,5,1),
        }
        self.v_walls = {
            (1,5,7),
            (1,1,3),
            (11,5,5),
            (11,11,1),
            (10,10,1),
            (8,5,7),
            (6,1,3),
            (5,1,3),
        }

        ceiling_entries = [
            Entry('C', (2, 8), 'X', False),
            Entry('C', (4, 10), 'X', False),
            Entry('C', (6, 8), 'X', False),
            Entry('C', (4, 6), 'X', False),
        ]

        def lever_trigger():
            for e in ceiling_entries:
                e.toggle()

        self.things = [
            Target((5, 13)),
            Lever(lever_trigger, (11, 0)),
            Entry('W', (0, 11), 'B', True),
            Entry('W', (10, 11), 'B', True),
            Entry('W', (11, 11), 'B', True),
            Box((10, 10)),
            Guard((7, 4)),
        ] + ceiling_entries

        super().__init__()
