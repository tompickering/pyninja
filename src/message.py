class Message(object):
    typ = 'NULL'

    typs = [
        'NULL',
        'OBJ',
        'KILL',
        'EVTS',
        'ACT_CLASS',
    ]

    attach = None

    def __init__(self, typ, attach=None):
        assert typ in Message.typs
        self.typ = typ
        self.attach = attach
