import os
import sys
import pygame
import logging

from pygame.locals import *

from consts import (WIN_POS, WIN_SZ, FPS)

from levels.test_level_00 import TestLevel00
from draw.game_draw_manager import GameDrawManager
from things import *
from world import World

from things.thing import Thing

logger = logging.getLogger()

def setup_logging():
    level = '--debug' in sys.argv and logging.DEBUG or logging.INFO
    logger.setLevel(level)
    ch = logging.StreamHandler(sys.stderr)
    formatter = logging.Formatter('[%(levelname)s] %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    logger.info('Logging set to ' + logging.getLevelName(level))

class Game(object):

    def __init__(self):
        self.world = None
        self.mouseover_obj = None

        pygame.init()
        self.mouse_pos = [0, 0]
        os.environ['SDL_VIDEO_WINDOW_POS'] = '%d,%d' % WIN_POS
        self.fpsClock = pygame.time.Clock()
        self.win = pygame.display.set_mode(WIN_SZ)
        self.surf = pygame.Surface(WIN_SZ)
        logger.info('Setting up draw manager...')
        self.draw_manager = GameDrawManager()
        logger.info('Starting game loop...')
        self.loop()

    def process_events(self):
        evts = pygame.event.get()
        for evt in evts:
            if evt.type == QUIT:
                return False
            elif evt.type == KEYDOWN:
                if evt.key == K_ESCAPE:
                    return False
                elif evt.key == K_SPACE:
                    self.world.toggle_time()

        for evt in evts:
            if evt.type == MOUSEMOTION:
                self.mouse_pos = evt.pos[:]
            #if evt.type == MOUSEBUTTONDOWN:
            #    self.world.click(evt.button)
        return evts


    def loop(self):
        running = True
        lev = TestLevel00()
        self.world = World()
        self.world.load_world(lev)
        self.draw_manager.update_world_ref(self.world)
        worlds = []
        last_selected = None

        while running:
            # Keep track of the currently-selected object
            if self.world.selected is not last_selected:
                last_selected = self.world.selected

            # Update the mouse position
            self.draw_manager.update_mouse_pos(self.mouse_pos)

            # Draw the screen
            view = self.draw_manager.draw()
            self.win.blit(view, (0, 0))
            pygame.display.update()

            mouseover_action = self.draw_manager.get_mouseover_action()
            mouseover_obj = self.draw_manager.get_mouseover_obj()
            mouse_block = self.draw_manager.get_mouse_block()
            self.world.update_mouseover_action(mouseover_action)
            self.world.update_mouseover_obj(mouseover_obj)
            self.world.update_mouse_block(mouse_block)

            # Process events
            evts = self.process_events()

            # End if not running any more
            if evts is False:
                running = False

            # Step time forwards
            self.world.input(evts)
            if self.world.time_is_progressing():
                self.world.timestep()

            self.fpsClock.tick(FPS)


        pygame.quit()

setup_logging()
logger.info('Creating game...')
Game()
