import re
import pygame
import logging

import os

from consts import (FONT_NAME_MAIN, FONTSIZE_S, FONTSIZE_M, FONTSIZE_L)

logger = logging.getLogger()

class DrawManager(object):
    spr = {}
    loaded_sprdirs = set()
    spr_frame_params = {}
    SPRFILE_EXTENSIONS = ('.png')

    col = {
        'black'  : pygame.Color(000,000,000),
        'red'    : pygame.Color(255,000,000),
        'green'  : pygame.Color(000,255,000),
        'blue'   : pygame.Color(000,000,255),
        'cyan'   : pygame.Color(000,255,255),
        'yellow' : pygame.Color(255,255,000),
        'magenta': pygame.Color(255,000,255),
        'white'  : pygame.Color(255,255,255),
    }

    @classmethod
    def load_sprites(cls, path):
        if path not in cls.loaded_sprdirs:
            logger.info('Loading sprites at ' + path)
            for filename in os.listdir(path):
                # Only load image files.
                if not filename[-4:] in cls.SPRFILE_EXTENSIONS:
                    continue
                full_path = os.path.join(path, filename)
                cls.spr[full_path] = pygame.image.load(full_path)
            cls.loaded_sprdirs.add(path)

    @classmethod
    def get_frame_parameters(cls, sprdir, base):
        """
        Get information required about the sprites that we have
        for a given filename base. These include:
          * The maximum frame number for a given file prefix.
          * The number of digits in each (due to 0-padding)
        """
        # FIXME: Ensure this is done before gameplay starts!
        full_path = os.path.join(sprdir, base)
        if full_path in cls.spr_frame_params:
            return cls.spr_frame_params[full_path]
        max_frame = -1
        digits = 0
        # XXX: We don't check that the set of indices is contuguous!
        for path in cls.spr:
            if not path.startswith(full_path):
                continue
            m = re.search('(?<=' + full_path + '_)[0-9]+', path)
            if m:
                max_frame = max(max_frame, int(m.group(0)))
                digits = max(digits, len(m.group(0)))
        if max_frame < 0:
            logger.warning('No animation frames found for base ' + base)
            max_frame = None
            digits = None
        logger.debug('Found ' + str(max_frame) + ' frames for ' + base)
        # Cache the result
        cls.spr_frame_params[full_path] = (max_frame, digits)
        return max_frame, digits

    def load_fonts(self, path):
        logger.info('Loading fonts...')
        self.font_s = pygame.font.Font(path + FONT_NAME_MAIN, FONTSIZE_S)
        self.font_m = pygame.font.Font(path + FONT_NAME_MAIN, FONTSIZE_M)
        self.font_l = pygame.font.Font(path + FONT_NAME_MAIN, FONTSIZE_L)

    def __init__(self, size, img_path, font_path):
        self.world = None
        self.bg_col = None
        self.font_s = None
        self.font_m = None
        self.font_l = None
        self.mouseover_action = None
        self.mouseover_obj = None
        self.mouse_block = None
        self.mouse_block_px = None
        self.update_required = True
        self.sub_dms = []
        self.font = {}
        self.size = size
        self.surf = pygame.Surface(size)
        self.mouse_pos = []
        if img_path:
            self.load_sprites(img_path)
        if font_path:
            self.load_fonts(font_path)

    def mouse_in_area(self, x, y, w, h):
        mp = self.mouse_pos
        if not mp:
            return False
        return (mp[0] > x
                and mp[0] < x + w
                and mp[1] > y
                and mp[1] < y + h)

    def update_world_ref(self, world):
        self.world = world
        for dm in self.sub_dms:
            list(dm.keys())[0].update_world_ref(world)

    def sprite(self, thing):
        """
        Return suitable Pygame image object for this thing
        at this current moment in time.
        """
        sprfile = thing.sprite()
        if sprfile:
            full_path = os.path.join(thing.SPR_DIR, sprfile + '.png')
            return self.spr[full_path]

    def update_mouse_pos(self, pos):
        self.mouse_pos = pos[:]
        for dm in self.sub_dms:
            off = list(dm.values())[0]
            rel_pos = [pos[i] - off[i] for i in [0,1]]
            list(dm.keys())[0].update_mouse_pos(rel_pos)

    def point_in_self(self, p):
        if p[0] >= 0 and p[0] + 1 < self.size[0]:
            if p[1] >= 0 and p[1] + 1 < self.size[1]:
                return True
        return False

    def get_mouseover_action(self):
        """
        Return class which relates to the action
        whose button the mouse is currently over,
        or otherwise None.
        """
        a = self.mouseover_action
        for dm in self.sub_dms:
            a = a or list(dm.keys())[0].get_mouseover_action()
        return a

    def get_mouseover_obj(self):
        o = self.mouseover_obj
        for dm in self.sub_dms:
            o = o or list(dm.keys())[0].get_mouseover_obj()
        return o

    def get_mouse_block(self, pixels=False):
        o = pixels and self.mouse_block_px or self.mouse_block
        for dm in self.sub_dms:
            o = o or list(dm.keys())[0].get_mouse_block(pixels=pixels)
        return o

    def mouse_on_thing(self, thing):
        ix = thing.pos[0] + thing.selrect[0]
        iy = thing.pos[1] + thing.selrect[1]
        iw = thing.selrect[2]
        ih = thing.selrect[3]
        mx, my = self.mouse_pos
        if mx > ix and mx < ix + iw:
            if my > iy and my < iy + ih:
                return True
        return False

    def needs_updating(self):
        return self.update_required

    def get_surf(self):
        return self.surf

    def draw(self):
        self.mouseover_obj = None
        if self.bg_col:
            self.surf.fill(self.bg_col)
        for sub_dm in self.sub_dms:
            assert len(sub_dm.keys()) == 1
            dm  = list(sub_dm.keys())[0]
            pos = list(sub_dm.values())[0]
            if dm.needs_updating():
                dm_surf = dm.draw()
            else:
                dm_surf = dm.get_surf()
            self.surf.blit(dm_surf, pos)
        return self.surf
