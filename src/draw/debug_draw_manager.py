import pygame

from draw_manager import DrawManager


class DebugDrawManager(DrawManager):

    def __init__(self):
        self.size = (1000, 640)
        self.spr_path  = 'src/img/world/'
        self.font_path = 'src/fonts/'
        self.bg_col = pygame.Color(000, 000, 000)
        super().__init__(self.size,
                         self.spr_path,
                         self.font_path)

    def draw_time(self):
        tstate = 'progressing.' if self.world.time_is_progressing() else 'stopped.'
        col = self.col['green'] if self.world.time_is_progressing() else self.col['red']
        tsurf = self.font_m.render('Time is ' + tstate, False, col)
        self.surf.blit(tsurf, (0, 0))

    def draw(self):
        super().draw()
        self.draw_time()
        return self.surf
