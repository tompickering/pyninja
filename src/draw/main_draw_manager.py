import os
import pygame

from draw_manager import DrawManager
from world_draw_manager import WorldDrawManager
from consts import BS


class MainDrawManager(DrawManager):

    def __init__(self):
        self.size = (505, 505)
        self.spr_path  = 'src/img/world/'
        self.font_path = 'src/fonts/'
        super().__init__(self.size,
                         self.spr_path,
                         self.font_path)
        self.mouse_block_col = self.col['red']
        self.sub_dms.append({WorldDrawManager(): (0, 0)})
        self.mouse_block_rect = [0, 0]

    def draw_path(self, p):
        if not p:
            return
        idx = 1
        while idx < len(p):
            pygame.draw.line(self.surf,
                             self.col['blue'],
                             p[idx-1],
                             p[idx],
                             3)
            idx += 1

    def get_marker(self, thing):
        if thing is self.world.selected:
            return self.spr[os.path.join(self.spr_path, 'sl.png')]
        if thing is self.world.highlighted:
            return self.spr[os.path.join(self.spr_path, 'hl.png')]

    def draw_things(self):
        for thing in self.world.things + self.world.ninjas:
            spr = self.sprite(thing)
            marker_spr = self.get_marker(thing)
            if thing.__class__.__name__ == 'Ninja' and marker_spr:
                self.draw_path(thing.get_path())
            if spr:
                self.surf.blit(spr, [thing.pos[0] + thing.draw_offset[0],
                                     thing.pos[1] + thing.draw_offset[1]])
            elif thing.__class__.__name__ == 'Entry':
                entry = thing
                pos = list(entry.pos)
                bso2 = BS // 2
                pos[0] += bso2
                pos[1] += bso2
                if entry.type == 'W':
                    if entry.mnt == 'T':
                        pos[1] -= bso2
                    elif entry.mnt == 'B':
                        pos[1] += bso2
                    elif entry.mnt == 'L':
                        pos[0] -= bso2
                    elif entry.mnt == 'R':
                        pos[0] += bso2
                col = self.col['red']
                if entry.open:
                    col = self.col['green']
                if entry is self.world.highlighted:
                    col = self.col['yellow']
                pygame.draw.circle(self.surf, col, pos, 8, 0)
            if marker_spr:
                self.surf.blit(marker_spr, thing.pos)
            if self.mouse_on_thing(thing):
                self.mouseover_obj = thing

    def draw(self):
        super().draw()
        self.mouse_block    = [int(x / BS) for x in self.mouse_pos]
        self.mouse_block_px = [x * BS for x in self.mouse_block]
        draw_mouse_sq = self.point_in_self(self.mouse_block_px)
        self.mouse_block    = draw_mouse_sq and self.mouse_block    or None
        self.mouse_block_px = draw_mouse_sq and self.mouse_block_px or None
        self.draw_things()
        if draw_mouse_sq:
            wh = [BS] * 2
            mouse_draw_rect = pygame.Rect(self.mouse_block_px, wh)
            pygame.draw.rect(self.surf, self.mouse_block_col, mouse_draw_rect, 2)
        return self.surf
