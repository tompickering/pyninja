from draw_manager import DrawManager

import pygame

from consts import (BOX_XPAD, BOX_YPAD, BOX_YSEP, FIRST_BOX_Y, ANTIALIAS)


class PanelDrawManager(DrawManager):

    def __init__(self):
        self.names_to_boxes   = None
        self.size      = (280,505)
        self.spr_path  = 'src/img/world/'
        self.font_path = 'src/fonts/'
        super().__init__(self.size, self.spr_path, self.font_path)
        self.names_to_boxes = {}
        self.names_to_classes = {}

    def get_action_at(self, cp):
        for n in self.names_to_boxes.keys():
            box = self.names_to_boxes[n]
            if box.x < cp[0] and box.x + box.w > cp[0]:
                if box.y < cp[1] and box.y + box.h > cp[1]:
                    name = n
                    break
        else:
            return None
        return self.names_to_classes[name]

    def draw_box(self, win, ts, tx, pos, w, h, col='white'):
        pygame.draw.rect(win, self.col[col], (pos[0], pos[1], w, h), 1)
        win.blit(ts, (tx, pos[1] + BOX_YPAD))

    def draw_boxes(self, win, boxes, box_y):
        txtsurfs = []
        box_h = 0
        max_w = 0
        strs = []
        for box in boxes:
            strs.append(box)
            txtsurf = self.font_m.render(box, ANTIALIAS, self.col['white'])
            txt_w = txtsurf.get_width()
            if txtsurf.get_height() > box_h:
                box_h = txtsurf.get_height()
            txtsurfs.append(txtsurf)
            if max_w < txt_w:
                max_w = txt_w

        box_h += 2 * BOX_YPAD
        box_x = win.get_width() / 2 - max_w / 2 - BOX_XPAD
        box_w = max_w + 2 * BOX_XPAD

        self.mouseover_action = None
        for idx, ts in enumerate(txtsurfs):
            txt_x = win.get_width() / 2 - ts.get_width() / 2
            cls = self.names_to_classes[strs[idx]]
            col = cls == self.world.selected_action and 'red' or 'white'
            self.draw_box(win, ts, txt_x, (box_x, box_y), box_w, box_h, col=col)
            if self.mouse_in_area(box_x, box_y, box_w, box_h):
                self.mouseover_action = self.names_to_classes[strs[idx]]
            self.names_to_boxes[strs[idx]] = pygame.rect.Rect(box_x, box_y,
                                                              box_w, box_h)
            box_y += box_h + BOX_YSEP

    def draw(self):
        super().draw()
        win = self.surf
        obj = self.world.selected
        win.fill(0)
        self.names_to_boxes = {}
        self.names_to_classes = {}
        objname = '.'*10
        if obj:
            objname = obj.__class__.__name__
        txtsurf = self.font_l.render(objname, ANTIALIAS, self.col['red'])
        txt_w = txtsurf.get_width()
        win_w = win.get_width()
        win.blit(txtsurf, (win_w/2 - txt_w/2, 20))
        box_names = []

        if hasattr(obj, 'available_actions'):
            if obj.available_actions:
                box_names = []
                for b in obj.available_actions:
                    box_names.append(b.__name__)
                    self.names_to_classes[b.__name__] = b

        if len(box_names):
            self.draw_boxes(win, box_names, FIRST_BOX_Y)

        return self.surf
