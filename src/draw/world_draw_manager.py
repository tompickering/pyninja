import pygame

from draw_manager import DrawManager
from consts import BS


class WorldDrawManager(DrawManager):

    def __init__(self):
        self.SIZE = (505, 505)
        self.SPR_PATH  = 'src/img/world/'
        self.FONT_PATH = 'src/fonts/'
        super().__init__(self.SIZE, self.SPR_PATH, self.FONT_PATH)

    def draw_walls(self):
        for w in self.world.walls:
            wall = [wv * BS for wv in w]
            wall[::2]  = [x for x in wall[::2]]
            wall[1::2] = [x for x in wall[1::2]]
            pygame.draw.line(self.surf, pygame.Color(255,255,255),
                    (wall[0],wall[1]),(wall[2],wall[3]), 3)

    def draw_world(self):
        hrange = range(0, self.world.size[0], BS)
        vrange = range(0, self.world.size[1], BS)
        for j in vrange:
            for i in hrange:
                self.surf.blit(self.spr['src/img/world/floor_placeholder.png'], (i, j))

    def draw(self):
        super().draw()
        self.draw_world()
        self.draw_walls()
        self.update_required = False
        return self.surf
