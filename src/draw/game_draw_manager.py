import pygame

from draw_manager import DrawManager
from debug_draw_manager  import DebugDrawManager
from main_draw_manager  import MainDrawManager
from panel_draw_manager import PanelDrawManager


class GameDrawManager(DrawManager):

    def __init__(self):
        self.size = (1000, 640)
        super().__init__(self.size, None, None)
        self.sub_dms.append({ DebugDrawManager(): (000, 000)})
        self.sub_dms.append({  MainDrawManager(): (100, 100)})
        self.sub_dms.append({ PanelDrawManager(): (650, 100)})
