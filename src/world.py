import logging

from pygame.locals import *
from consts import TIMESTEP

from things.ninja import Ninja
from util import rationalise_path

logger = logging.getLogger()


class World(object):

    def __init__(self):
        self.size = [0, 0]
        self.things = []
        self.walls = []
        self.ninjas = []
        self.level = None
        self.mouse_block = None
        self.selected = None
        self.highlighted = None
        self.selfref = None
        self.time = 0.
        self.time_is_playing = True
        self.mouseover_action = None
        self.selected_action = None

    def toggle_time(self):
        self.time_is_playing = not self.time_is_playing

    def time_is_progressing(self):
        return self.time_is_playing

    def load_world(self, level):
        logger.info('Preparing world...')
        self.level = level
        self.things = []
        self.size = level.size[:]
        self.things = level.things[:]
        self.walls = level.walls[:]
        self.ninjas = []

    def shortest_path(self, start, target, ignore_things=None):
        """
        Given a start and end-point, find the shortest path
        :param start, target: 2-tuple with block co-ordinates
        :param ignore_things: class or list of classes of things to ignore,
                              e.g. so ninjas can pass one another
        :return: list of 2-tuples (waypoints)
        """
        if not isinstance(ignore_things, list):
            ignore_things = [ignore_things]

        if start == target:
            return [start]

        def adjacent_blocks(p):
            """
            Given a point p, return a set of blocks which are
            adjacent and reachable
            """
            # Find all possibilities
            possibilities = {
                (p[0], p[1] + 1),
                (p[0], p[1] - 1),
                (p[0] + 1, p[1]),
                (p[0] - 1, p[1]),
            }
            # Eliminate areas with things in
            possibilities -= {t.block_pos for t in self.things if t.solid
                              and not any(isinstance(t, cls) for cls in ignore_things)}
            # Eliminate areas blocked by walls
            for w in self.walls:
                if (w[0] == w[2] # V wall
                    and w[0] in (p[0], p[0] + 1) # To the immediate left or right of p's column
                    and p[1] + .5 == sorted([w[1], w[3], p[1] + .5])[1]): # And p is vertically between the ends
                    # One of these will be p depending on whether which one of
                    # (p[0], p[0] + 1) w[0] was equal to, but it's not in the set so it doesn't matter
                    possibilities -= {(w[0], p[1]), (w[0] - 1, p[1])}
                elif (w[1] == w[3] # H wall
                    and w[1] in (p[1], p[1] + 1) # On the row immediately above or below p
                    and p[0] + .5 == sorted([w[0], w[2], p[0] + .5])[1]): # And p is horizontally between the ends
                    # One of these will be p depending on whether which one of
                    # (p[1], p[1] + 1) w[1] was equal to, but it's not in the set so it doesn't matter
                    possibilities -= {(p[0], w[1]), (p[0], w[1] - 1)}
            return possibilities

        seen_blocks = set()

        def extend_tree(paths):
            """
            Take a list of paths and return added 1 place to each of them
            """
            nonlocal seen_blocks
            extended = False
            new_paths = []
            for path in paths:
                new_blocks = adjacent_blocks(path[-1])
                new_blocks -= set(path)
                new_blocks -= seen_blocks
                seen_blocks |= new_blocks
                if target in new_blocks:
                    return path + [target]
                for new_block in new_blocks:
                    extended = True
                    new_paths.append(path + [new_block])
            if not extended:
                # We couldn't extend any path. Conclusion:
                # the target is not reachable.
                return None
            return extend_tree(new_paths)

        return rationalise_path(extend_tree([[start]]))

    def click(self, button):
        """
        Register a click in the world, and pass the event on
        to the appropriate object(s).
        :param button: int, 1 for left-click, 3 for right-click
        """
        thing = self.highlighted
        action = self.mouseover_action
        if action:
            logger.debug('Click {} registered on action {}'.format(button, action))
            self.selected_action = action
            # Some actions have immediate effect;
            # give the thing a chance to do something
            if self.selected:
                self.selected.click_action(self, action)
        elif thing:
            logger.debug('Click {} registered on thing {}'.format(button, thing))
        else:
            logger.debug('Click {} registered on nothing exciting'.format(button))
        if thing and thing.selectable and button == 1:
            self.selected = thing
            # Set the default action
            if hasattr(thing, 'available_actions') and thing.available_actions:
                self.selected_action = thing.available_actions[0]
        if not thing and self.mouse_block and button == 1:
            self.selected = None
        if thing and button == 1:
            thing.click_left(self)
        if self.selected and self.mouse_block and button == 3:
            self.selected.click_right(self, thing, self.mouse_block)

    def enter_the_ninja(self, new_ninja):
        logger.info('Spawning ninja')
        self.things.append(new_ninja)
        self.ninjas.append(new_ninja)

    def update_mouseover_obj(self, o):
        if o is None or (o.highlightable and o is not self.highlighted):
            logger.debug('Mouseover object changed: ' + str(o))
            self.highlighted = o

    def update_mouseover_action(self, a):
        if a is not self.mouseover_action:
            logger.debug('Mouseover action changed: ' + str(a))
            self.mouseover_action = a

    def update_mouse_block(self, b):
        if b != self.mouse_block:
            self.mouse_block = b

    def input(self, evts):
        evts = evts or []
        for evt in evts:
            if evt.type == MOUSEBUTTONDOWN:
                self.click(evt.button)

    def timestep(self):
        for thing in self.things:
            thing.timestep(self)
        self.time += TIMESTEP
