import sys

# Set up import paths
for import_dir in ['', '/draw', '/things']:
    sys.path.insert(0, 'src' + import_dir)

try:
    import main
except KeyboardInterrupt:
    from sys import stderr
    stderr.write('\nThanks for playing! Goodbye.\n')
