def rationalise_path(path):
    """
    Given a path p, condense it into a series of
    axis-aligned waypoints.
    E.g. [(0,0),(0,4),(0,9),(4,9),(5,9)] -> [(0,0),(0,9),(5,9)]
    NOTE this removes doubling-back
    """
    if path is None:
        return
    if not path:
        return path[:]
    rationalised_path = []
    d = None
    for i, point in enumerate(path):
        if not i:
            continue
        last_point = path[i - 1]
        if last_point[0] == point[0] and d == 'v':
            continue
        elif last_point[1] == point[1] and d == 'h':
            continue
        else:
            # Changed direction
            d = last_point[0] == point[0] and 'v' or 'h'
            rationalised_path.append(last_point)
    rationalised_path.append(path[-1])
    return rationalised_path

def adjacent(b1, b2):
    """
    True if blocks 1 and 2 are adjacent
    """
    return b2 in {
        (b1[0] + 1, b1[1]),
        (b1[0] - 1, b1[1]),
        (b1[0], b1[1] + 1),
        (b1[0], b1[1] - 1),
    }
