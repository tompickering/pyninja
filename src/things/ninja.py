from thing import Thing
from shuriken import Shuriken
from actor import Actor

import pygame

from action import *
from action.walk import Walk
from action.activate import Activate
from action.throw import Throw
from action.lift import Lift

from consts import BS
from util import adjacent


class Ninja(Actor):

    SPR_DIR = 'src/img/'
    SPRFILE_BASE = 'ninja'

    ANIM_ARGS = {
        'idle' : {
            'base'      : 'idle',
            'upf'       : 40,
        },
        'walk' : {
            'base'      : 'ninja',  # FIXME: This should be 'walk' - sprite name derivation is incorrect
            'upf'       : 10,
        },
    }

    def __init__(self, pos):
        super().__init__(pos, self.ANIM_ARGS['idle'])
        self.highlightable = True
        self.selectable = True
        self.spr_ctr = 0
        self.spr_ctr_max = 1

        self.available_actions = [
            Walk,
            Activate,
            Lift,
            Throw,
        ]

    def click_left(self, world):
        pass

    def click_right(self, world, thing, mouse_block):
        a = world.selected_action
        if a is Activate:
            # Interact with thing
            # Only if where we are going to end up after all our actions
            # (self.future.block_pos, block co-ords) is next to the thing
            if adjacent(thing.block_pos, self.future.block_pos):
                self.add_action(Activate(thing))
        elif a is Throw:
            m = tuple(mouse_block)
            obj = self.future.carried_object
            if obj:
                self.add_action(Throw(obj.real, m, self, toss=True))
            else:
                projectile = Shuriken(self.future.block_pos)
                self.add_action(Throw(projectile, m))
        elif a is Lift and thing and thing.liftable:
            if adjacent(thing.block_pos, self.future.block_pos):
                self.add_action(Lift(thing))
        elif a is Walk:
            m = tuple(mouse_block)

            # Run there! Or walk / flip / jump / crawl / roll / wall-press?
            path = world.shortest_path(self.future.block_pos, m, ignore_things=Ninja)
            if path:
                self.future.pos = (x * BS for x in path[-1])
                walk = Walk(path, self.ANIM_ARGS['walk'])
                self.add_action(walk)
