from thing import Thing

import logging

logger = logging.getLogger()

class Trigger(Thing):

    def __init__(self, trigger_func, *a, **kw):
        self.trigger_func = trigger_func
        super().__init__(*a, **kw)

    def activate(self):
        logger.debug('Trigger activating')
        if self.trigger_func:
            self.trigger_func()
        else:
            logger.warning('Trigger activated but no function set')
