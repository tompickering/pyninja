from thing import Thing

from consts import BS
from copy import copy
from action.walk import Walk


class Actor(Thing):
    """
    Any Thing which is capable of carrying out actions
    """

    def __init__(self, pos, anim_args=None):
        self.action_mode = None
        super().__init__(pos, anim_args)
        self.actions = []
        self.available_actions = []
        self.carried_object = None
        self._future = None

    def add_action(self, act):
        self._future = None
        self.actions.append(act)

    def current_action(self):
        return self.actions and self.actions[0] or None

    def set_action_mode(self, act_class, world):
        super().set_action_mode(act_class, world)
        self.action_mode = act_class

    def timestep(self, world):
        lact = self.current_action()
        if lact and lact.complete:
            self._future = None
        self.actions = [act for act in self.actions if not act.complete]
        lact = self.current_action()
        if lact:
            lact.update(self, world)
        if self.anim and (not lact or not lact.anim):
            super().timestep(world)
            self.anim.update()

    def get_anim(self):
        curr_act = self.current_action()
        return curr_act and curr_act.anim or self.anim

    def get_path(self):
        walk = [act for act in self.actions if isinstance(act, Walk)]
        p = [[self.pos[i] + BS / 2 for i in [0,1]]]
        for w in walk:
            for wp in w.path:
                p.append([wp[i] + BS / 2 for i in [0,1]])
        return p

    def carry(self, obj):
        """
        Start carrying the given object
        """
        self.put_down()
        self.carried_object = obj
        # Note this sets a *reference* to the carrier's pos!
        obj.pos = self.pos
        obj.pick_up()

    def put_down(self):
        """
        Put down any carried object
        """
        if self.carried_object:
            self.carried_object.put_down()
        obj = self.carried_object
        self.carried_object = None
        if obj:
            # Ensure the once-carried object now has its own pos!
            obj.pos = self.pos[:]
        return obj

    def sprite(self):
        anim = self.get_anim()
        return anim and anim.sprite() or None

    @property
    def future(self):
        """
        Calculate this object as it will be following its queue of actions
        :return: Actor representing the future state of this one
        """
        if self._future:
            return self._future
        self._future = self.copy()
        for action in self.actions:
            action.apply_future(self._future)
        return self._future
