from trigger import Trigger


class Lever(Trigger):
    SPR_DIR = 'src/img/'
    SPRFILE_BASE = 'lever'

    ANIM_ARGS = {
        'off' : {
            'base'      : 'off',
            'upf'       : 40,
        },
        'on' : {
            'base'      : 'on',
            'upf'       : 40,
        },
    }

    def __init__(self, trigger_func, pos, *a, **kw):
        super().__init__(trigger_func, pos, self.ANIM_ARGS['off'], *a, **kw)
        self.highlightable = True
        self.selectable = False
        self.spr_ctr = 0
        self.spr_ctr_max = 0
        self.on = False

    def activate(self):
        super().activate()
        self.on = not self.on
        self.update_anim_args(self.ANIM_ARGS[self.on and 'on' or 'off'])

    def click_left(self, world):
        pass

    def click_right(self, world, thing, mouse_block):
        pass
