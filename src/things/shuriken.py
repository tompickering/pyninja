from things.thing import Thing
from things.actor import Actor

from things.action.fly import Fly

from consts import BS


class Shuriken(Actor):

    SPR_DIR = 'src/img/'
    SPRFILE_BASE = 'shuriken'

    ANIM_ARGS = {
        'shuriken': {
            'base': 'shuriken',
            'upf': 40,
        },
    }

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.highlightable = False
        self.selectable = False
        self.solid = False
        self.available_actions = [Fly]
        self.update_anim_args(self.ANIM_ARGS['shuriken'])

    def click_left(self, world):
        pass

    def click_right(self, world, thing, mouse_block):
        pass
