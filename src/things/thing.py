import copy
import logging

import pygame
from pygame import Rect

from consts import BS

from draw_manager import DrawManager
from anim.animation import Animation

from sys import stdout

logger = logging.getLogger()


# Ensure that the draw manager loads each Thing's sprites.
def load_sprites(cls):
    if cls.SPR_DIR:
        DrawManager.load_sprites(cls.SPR_DIR)
    else:
        logger.warning("Class '%s' does not define SPR_DIR" % cls.__name__)


class Thing(object):
    SPR_DIR = ''
    SPRFILE_BASE = ''

    @property
    def block_pos(self):
        return (self.pos[0] // BS, self.pos[1] // BS)

    def update_anim_args(self, anim_args):
        self.anim = None
        if anim_args:
            aa = anim_args.copy()
            aa['base'] = self.SPRFILE_BASE + '_' + aa['base']
            aa['frame_max'], aa['digits'] = DrawManager.get_frame_parameters(self.SPR_DIR, aa['base'])
            self.anim = Animation(**aa)

    def __init__(self, pos, anim_args):
        self.pos = [pos[0] * BS, pos[1] * BS]
        self.anim = None
        self.selectable = False
        self.highlightable = False
        self.liftable = False
        self.solid = True  # Ninjas etc cannot move through this
        self.draw_offset = [0, 0]
        logger.info('Creating new ' + str(self.__class__.__name__) +
                    ' at ' + str(self.block_pos))

        # Relative to position
        # Defines where the mouse has to be for this to be highlighted
        # Default selection rectangle is 75% block size
        self.selrect = [BS/8, BS/8, 3*BS/4, 3*BS/4]

        self.update_anim_args(anim_args)

        # Each non-abstract Thing class should define
        # a SPR_DIR pointing to its sprite repository.
        # Ensure that the draw manager loads these.
        load_sprites(self.__class__)
        self.real = self

    def copy(self, deep=False):
        """
        Create and return a copy of this object which contains a
        reference to the original.
        """
        copy_func = copy.deepcopy if deep else copy.copy
        cp = copy_func(self)
        cp.real = self
        return cp

    def click_left(self, world):
        """
        A left click has happened in the world while this Thing
        is selected. Arrange for the necessary actions.
        """
        pass

    def click_right(self, world, thing, mouse_block):
        """
        A right click has happened in the world while this Thing
        is selected. Arrange for the necessary actions.
        """
        pass

    def click_action(self, world, thing):
        """
        An action has been selected
        """
        pass

    def timestep(self, world):
        """
        Register a progression of time
        """
        if self.anim:
            self.anim.update()

    def set_action_mode(self, to_ignore, world):
        pass

    def pick_up(self):
        """
        We've been picked up
        """
        self.draw_offset = [0, -15]

    def put_down(self):
        """
        We've been put down
        """
        self.draw_offset = [0, 0]

    def sprite(self):
        """
        Get the appropriate sprite name for this thing currently,
        given its state and currently performed action.
        Note .png suffix is not present.
        """
        return self.anim.sprite()

