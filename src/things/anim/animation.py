class Animation(object):
    def __init__(self, base, frame_max, digits, upf):
        """
        :param base: sprite base
        :param frame_max: highest frame number
        :param digits: number of digits in file frame number (0-padded)
        :param upf: number of game updates before the frame is advanced
        """
        self.base = base
        self.frame_max = frame_max
        self.upd_per_frame = upf
        self.frame = 0
        self.digits = 0
        self.frame_upd = 0

    def update(self):
        """
        Register a game update and progress the animation if needed
        """
        self.frame_upd += 1
        if self.frame_upd >= self.upd_per_frame:
            self.frame += 1
            self.frame_upd = 0
            if self.frame > self.frame_max:
                self.frame = 0

    def sprite(self):
        """
        Get the full path for the current sprite
        """
        return self.base + ('_%%0%sd' % self.digits) % self.frame
