from actor import Actor

import pygame

from consts import BS
from util import adjacent


class Box(Actor):
    SPR_DIR = 'src/img/'
    SPRFILE_BASE = 'box'

    ANIM_ARGS = {
        'idle' : {
            'base'      : 'idle',
            'upf'       : 2,
        },
    }

    def __init__(self, pos):
        super().__init__(pos, self.ANIM_ARGS['idle'])
        self.highlightable = True
        self.selectable = False
        self.liftable = True

    def click_left(self, world):
        pass

    def click_right(self, world, thing, mouse_block):
        pass
