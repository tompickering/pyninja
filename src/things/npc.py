from actor import Actor
from action.wait import Wait


class NPC(Actor):
    """
    Any NPC or non-playable Actor, which generates its own actions
    """

    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self._act_gen = self.get_next_action()
        self._action = None

    def current_action(self):
        act = self._action
        if act and not act.complete:
            return act
        self._action = next(self._act_gen)
        return self._action

    def get_next_action(self):
        """
        Generate a series of actions
        """
        while True:
            yield Wait(100)
