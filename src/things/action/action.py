from consts import TIMESTEP
from draw_manager import DrawManager
from anim.animation import Animation

class Action(object):
    ANIM_BASE = ''

    def __init__(self, anim_args=None):
        self.complete = False
        self.anim = None
        self.started = False
        if anim_args:
            aa = anim_args.copy()
            sprbase = aa['base']
            aa['base'] = sprbase + '_' + self.ANIM_BASE
            # FIXME: Don't hard-code sprite dir!
            aa['frame_max'], aa['digits'] = DrawManager.get_frame_parameters('src/img/', aa['base'])
            self.anim = Animation(**aa)

    def update(self, thing, world):
        if not self.started:
            self.started = True
        if self.anim:
            self.anim.update()

    def apply_future(self, actor):
        """
        Apply any state changes as a result of this action to the actor
        """
        pass
