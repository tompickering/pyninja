import logging

from action.action import Action

logger = logging.getLogger()

class Wait(Action):

    def __init__(self, duration, *a, **kw):
        self.start = None
        self.duration = duration
        super().__init__(*a, **kw)

    def update(self, thing, world, *a, **kw):
        super().update(thing, world, *a, **kw)
        if self.start is None:
            # First update - begin waiting
            self.start = world.time
        if world.time >= self.start + self.duration:
            self.complete = True
