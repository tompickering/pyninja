import math
import logging

from action.action import Action

from consts import BS

logger = logging.getLogger()

TOSS_HEIGHT = 0.5

class Fly(Action):

    ANIM_BASE = ''

    def __init__(self, target, anim_args=None, toss=False):
        """
        Target in block co-ords
        If tossing, object will follow an arc
        """
        logger.debug('Creating fly: ' + str(target))
        self.spd = 24
        self.target = target
        self.tgt_px = (target[0] * BS, target[1] * BS)
        self.toss = toss
        self.total_dist = None
        self.max_height = None
        super().__init__(anim_args)

    def apply_future(self, actor):
        actor.pos = self.tgt_px

    def update(self, thing, world):
        super().update(thing, world)
        pos = thing.pos
        tgt_px = self.tgt_px
        dist = math.sqrt((tgt_px[0] - pos[0])**2 + (tgt_px[1] - pos[1])**2)

        # Set total_dist on first update
        if self.total_dist is None:
            self.total_dist = dist
        if self.max_height is None:
            self.max_height = self.total_dist * TOSS_HEIGHT

        if self.toss:
            # Calculate draw offset
            dist_done = self.total_dist - dist
            prop_done = dist_done / self.total_dist
            arc_len = 2 * math.sqrt(self.max_height)
            prop_arc = arc_len * prop_done
            y_off = self.max_height - ((prop_arc - math.sqrt(self.max_height)) ** 2)
            thing.draw_offset = [0, -y_off]

        if dist <= self.spd:
            thing.pos = tgt_px
            if self.toss:
                thing.draw_offset = [0, 0]
            self.complete = True
            return

        if thing.pos[0] == tgt_px[0]:
            if thing.pos[1] > tgt_px[1]:
                # Directly below target
                thing.pos[1] -= self.spd
            elif thing.pos[1] < tgt_px[1]:
                # Directly below target
                thing.pos[1] += self.spd
        else:
            opp = tgt_px[1] - pos[1]
            adj = tgt_px[0] - pos[0]
            angle = math.atan(opp / adj)
            d_opp = self.spd * math.sin(angle)
            d_adj = self.spd * math.cos(angle)
            if thing.pos[0] < tgt_px[0]:
                thing.pos[0] += d_adj
                thing.pos[1] += d_opp
            elif thing.pos[0] > tgt_px[0]:
                thing.pos[0] -= d_adj
                thing.pos[1] -= d_opp

        # TODO: Check for walls, collisions etc
