import logging

from action.action import Action
from action.fly import Fly

from thing import Thing
from consts import BS

logger = logging.getLogger()

class Throw(Action):

    ANIM_BASE = ''

    def __init__(self, projectile, target, actor=None, anim_args=None, toss=False):
        """
        There are two types of throw; equipped object and liftable item.
        If throwing liftable item, toss is True.
        """
        logger.debug('Throwing: ' + str(projectile) + ' at ' + str(target))
        self.projectile = projectile
        self.target = target
        self.toss = toss
        self.actor = actor
        super().__init__(anim_args)

    def apply_future(self, actor):
        # Whatever the case, after throwing, there is going to be
        # no carried item
        actor.put_down()

    def update(self, thing, world):
        if not self.started and self.actor:
            self.actor.put_down()
        super().update(thing, world)
        if not self.toss:
            # If not tossing, we're adding a new object to the world
            world.things.append(self.projectile)
        self.projectile.add_action(Fly(self.target, toss=self.toss))
        self.complete = True
