import logging

from action.action import Action

from thing import Thing
from consts import BS

logger = logging.getLogger()

class Lift(Action):
    ANIM_BASE = ''

    def __init__(self, target, anim_args=None):
        logger.debug('Lifting: ' + str(target))
        self.target = target
        super().__init__(anim_args)

    def apply_future(self, actor):
        actor.carry(self.target.copy(deep=True))

    def update(self, thing, world):
        super().update(thing, world)
        thing.carry(self.target)
        self.complete = True
