import logging

from action.action import Action

from thing import Thing
from consts import BS

logger = logging.getLogger()

class Walk(Action):

    ANIM_BASE = 'walk'

    def __init__(self, path, anim_args):
        logger.debug('Creating walk: ' + str(path))
        self.spd = 3 # PIXELS PER UPDATE

        super().__init__(anim_args)
        self.path = [(p[0] * BS, p[1] * BS) for p in path]
        self.path_orig = self.path[:]

    def apply_future(self, actor):
        if self.path:
            actor.pos = self.path[-1]

    def update(self, thing, world):
        super().update(thing, world)

        if not len(self.path):
            self.complete = True
            return

        nxt = self.path[0]

        if tuple(thing.pos) == nxt:
            del self.path[0]
            if len(self.path):
                nxt = self.path[0]
            else:
                nxt = None

        if not nxt:
            logger.debug('Completed walk')
            self.complete = True
            return

        idx = None
        if nxt[0] == thing.pos[0]:
            idx = 1
        elif nxt[1] == thing.pos[1]:
            idx = 0

        if idx is not None:
            if nxt[idx] < thing.pos[idx]:
                if thing.pos[idx] - self.spd >= nxt[idx]:
                    thing.pos[idx] -= self.spd
                else:
                    thing.pos[idx] = nxt[idx]
            elif nxt[idx] > thing.pos[idx]:
                if thing.pos[idx] + self.spd <= nxt[idx]:
                    thing.pos[idx] += self.spd
                else:
                    thing.pos[idx] = nxt[idx]
        else:
            assert False
