from action.action import Action

import logging

logger = logging.getLogger()

class Activate(Action):

    def __init__(self, thing, *a, **kw):
        self.target = thing
        self.duration = 10
        super().__init__(*a, **kw)

    def update(self, thing, world):
        """
        :param thing: Thing doing the activating
        """
        tgt = self.target
        if not hasattr(tgt, 'activate'):
            logger.error('Attempted to activate non-activatable ' + str(tgt))
            self.complete = True
            return
        tgt.activate()
        self.complete = True
