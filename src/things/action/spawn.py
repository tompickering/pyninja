from action.action import Action

class Spawn(Action):

    def __init__(self, obj, *a, **kw):
        self.obj = obj
        super().__init__(*a, **kw)

    def update(self, thing, world):
        if not self.complete:
            self.complete = True
            world.enter_the_ninja(self.obj)
