from npc import NPC


class Guard(NPC):

    SPR_DIR = 'src/img/'
    SPRFILE_BASE = 'guard'

    ANIM_ARGS = {
        'idle' : {
            'base'      : 'idle',
            'upf'       : 10,
        },
        'walk' : {
            'base'      : 'guard',  # FIXME: This should be 'walk' - sprite name derivation is incorrect
            'upf'       : 10,
        },
    }

    def __init__(self, pos):
        super().__init__(pos, self.ANIM_ARGS['idle'])
