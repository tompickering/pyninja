from thing import Thing
from actor import Actor
from ninja import Ninja

from action.wait import Wait
from action.spawn import Spawn

from consts import BS


class Entry(Actor):

    SPR_DIR = 'src/img/'
    SPRFILE_BASE = 'entry'

    ANIM_ARGS = {
        'ceiling_open': {
            'base': 'ceiling_open',
            'upf': 40,
        },
        'ceiling_closed': {
            'base': 'ceiling_closed',
            'upf': 40,
        },
    }

    def __init__(self, _type, pos, mnt, _open, *a, **kw):
        """
        :param pos: position
        :param mnt: (T)op, (B)ottom, (L)eft, (R)ight, X - Middle
        :param state: (C)losed, (O)pen
        :param _type = 'W' # ('W'all or 'C'eiling)
        """
        super().__init__(pos, *a, **kw)
        self.highlightable = True
        self.selectable = True
        self.available_actions = [Spawn]
        self.solid = False
        self.type = _type
        self.mnt = mnt
        self.open = _open
        if mnt == 'X':
            mnt = None
        self.selrect = [0, 0, 0, 0]
        if mnt in ['T', 'B']:
            self.selrect[0] = BS / 4
        elif mnt == 'L':
            self.selrect[0] = -BS / 4
        elif mnt == 'R':
            self.selrect[0] = 3 * BS / 4
        if mnt in ['L', 'R']:
            self.selrect[1] = BS / 4
        elif mnt == 'T':
            self.selrect[1] = -BS / 2
        elif mnt == 'B':
            self.selrect[1] = 3 * BS / 4
        if mnt is None:
            self.selrect[0] = BS / 4
            self.selrect[1] = BS / 4
        self.selrect[2] = self.selrect[3] = BS / 2
        self.update_entry_anim_args()

    def update_entry_anim_args(self):
        if self.open and self.type == 'C':
            super().update_anim_args(self.ANIM_ARGS['ceiling_open'])
        elif not self.open and self.type == 'C':
            super().update_anim_args(self.ANIM_ARGS['ceiling_closed'])
        else:
            self.update_anim_args(None)

    def toggle(self):
        self.open = not self.open
        self.update_entry_anim_args()

    def click_left(self, world):
        pass

    def click_right(self, world, thing, mouse_block):
        pass

    def click_action(self, world, action):
        if action is Spawn and self.open:
            self.add_action(Wait(15))
            self.add_action(Spawn(Ninja(self.block_pos)))

    def set_action_mode(self, mode, world):
        super().set_action_mode(mode, world)
        self.add_action(Wait(15))
        self.add_action(Spawn(Ninja(self.block_pos)))
