import logging

from things.entry import Entry
from things.lever import Lever
from things.target import Target

from consts import BS

logger = logging.getLogger()


class Level(object):

    def setup_walls(self):
        # Implicit bounding walls
        # FIXME: self.size should probably be in blocks already
        w = self.size[0] // BS
        h = self.size[1] // BS
        self.walls.append((0,0,w,0))
        self.walls.append((0,h,w,h))
        self.walls.append((0,0,0,h))
        self.walls.append((w,0,w,h))
        for w in self.h_walls:
            self.walls.append((w[0], w[1], (w[0] + w[2]), w[1]))
        for w in self.v_walls:
            self.walls.append((w[0], w[1], w[0], (w[1] + w[2])))

    def __init__(self):
        self.walls = []
        self.size = (self.size[0] * BS, self.size[1] * BS)
        self.setup_walls()
